import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    message = json.loads(body)
    email = message["presenter_email"]
    name = message["presenter_name"]
    title = message["title"]
    subject = "Your presentation has been accepted"
    message = f"{name}, we're happy to tell you that your presentation {title} has been accepted"
    from_email = "admin@conference.go"
    receipient_list = [email]

    send_mail(subject, message, from_email, receipient_list)

    print(f"Email sent to {email} for presentation approval")


def process_rejection(ch, method, properties, body):
    message = json.loads(body)
    email = message["presenter_email"]
    name = message["presenter_name"]
    title = message["title"]
    subject = "Your presentation has been rejected"
    message = f"{name}, we're sorry to tell you that your presentation {title} has been rejected"
    from_email = "admin@conference.go"
    receipient_list = [email]

    send_mail(subject, message, from_email, receipient_list)

    print(f"Email sent to {email} for presentation rejection")


parameters = pika.ConnectionParameters(host='rabbitmq')
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue="presentation_approvals")
channel.queue_declare(queue="presentation_rejections")
channel.basic_consume(
    queue="presentation_rejections",
    on_message_callback=process_rejection,
    auto_ack=True,
)
channel.basic_consume(
    queue="presentation_approvals",
    on_message_callback=process_approval,
    auto_ack=True,
)
channel.start_consuming()


# if __name__ == "__main__":
#     except KeyboardInterrupt:
#         print("Interrupted")
#         try:
#             sys.exit(0)
#         except SystemExit:
#             os._exit(0)
